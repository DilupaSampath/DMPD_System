import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    { path: '', redirectTo: 'Home', pathMatch: 'full' },
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'dashboard' },
            { path: 'Doctors', loadChildren: './dashboard/dashboard.module#DashboardModule', },
            { path: 'Nurses', loadChildren: './bs-element/bs-element.module#BsElementModule' },
            { path: 'Patients', loadChildren: './tables/tables.module#TablesModule' },
            { path: 'Home', loadChildren: './form/form.module#FormModule' },
            { path: 'Reports', loadChildren: './charts/charts.module#ChartsModule' },
            { path: 'blank-page', loadChildren: './blank-page/blank-page.module#BlankPageModule' }
        ]
    }

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
